package dk.centic.devicetracker.services;

import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.hardware.Camera;
import android.widget.Toast;

import com.google.zxing.integration.android.IntentIntegrator;

import dk.centic.devicetracker.R;

public class QRScannerService {

    private IntentIntegrator qrScan;
    private String qrCodeData;

    public QRScannerService(Activity activity){
        if (checkIfDeviceHasCamera(activity)){
            qrScan = new IntentIntegrator(activity);
            qrScan.setDesiredBarcodeFormats(IntentIntegrator.QR_CODE_TYPES);
            qrScan.setPrompt("Scanning");
            qrScan.setCameraId(0);
            qrScan.setOrientationLocked(false);
            qrScan.setBeepEnabled(false);
            qrScan.setBarcodeImageEnabled(false);
        } else {
            Toast.makeText(activity, R.string.inform_device_without_camera, Toast.LENGTH_SHORT).show();
        }
    }

    private boolean checkIfDeviceHasCamera(Context context){
        PackageManager packageManager = context.getPackageManager();
        return packageManager.hasSystemFeature(PackageManager.FEATURE_CAMERA);
    }

    void startScanning(){
        qrScan.initiateScan();
    }

    public String getUserId(){
        //TODO: return userId from parsed QR-code
        return qrCodeData;
    }

    public boolean qrCodeValidation(String qrCodeData){
        //TODO: parse qr-code
        this.qrCodeData = qrCodeData;
        return true;
    }
}
