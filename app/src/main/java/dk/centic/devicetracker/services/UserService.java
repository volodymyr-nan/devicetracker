package dk.centic.devicetracker.services;

import android.content.Context;

import java.util.ArrayList;
import java.util.List;

import dk.centic.devicetracker.db.repositories.UserRepository;
import dk.centic.devicetracker.models.User;

public class UserService {

    private UserRepository userRepository;

    public UserService(Context context){
        userRepository = new UserRepository(context);
    }

    public boolean addUser(String id, String name) {
        return userRepository.addUser(id, name);
    }

    public List<String> getUserNames() {
        List<User> users = userRepository.getUsers();
        List<String> usersName = new ArrayList<>();
        for (User user : users) {
            usersName.add(user.getName());
        }
        return usersName;
    }

    public List<User> getUsers() {
        return userRepository.getUsers();
    }

    public boolean activateUserById(String userId){
        return userRepository.activateUserById(userId);
    }

    public User getUserById(String userId){
        return userRepository.getUserById(userId);
    }

    public boolean stopTracking(){
        return userRepository.deactivateUser();
    }
}
