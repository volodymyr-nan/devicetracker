package dk.centic.devicetracker.services;

import android.app.Activity;
import android.appwidget.AppWidgetManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.google.zxing.integration.android.IntentResult;

import dk.centic.devicetracker.R;
import dk.centic.devicetracker.db.repositories.Repository;
import layout.UserListWidget;

public class UIService {

    private Context context;
    private ListView listViewUsers;

    private UserService userService;
    private QRScannerService qrScannerService;

    public UIService(Context context, ListView listView){
        this.context = context;
        listViewUsers = listView;
        userService = new UserService(context);
        qrScannerService = new QRScannerService((Activity) context);
    }

    public void startScanning(){
        qrScannerService.startScanning();
    }

    public void updateUserList(){
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(context, android.R.layout.simple_list_item_1, userService.getUserNames());
        listViewUsers.setAdapter(arrayAdapter);
    }

    public boolean іsQRScanningSuccessful(IntentResult result){
        if (result != null) {
            if (result.getContents() != null) {
                //if qr contains data
                return qrScannerService.qrCodeValidation(result.getContents());
            }
        }
        return false;
    }

    public void updateWidget(){
        Intent intent = new Intent(context, UserListWidget.class);
        intent.setAction(AppWidgetManager.ACTION_APPWIDGET_UPDATE);
        int[] ids = AppWidgetManager.getInstance(context).getAppWidgetIds(new ComponentName(context, UserListWidget.class));
        intent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS, ids);
        context.sendBroadcast(intent);
    }

    public String userCreationResult(String userName){

        if (userName.equals("")){
            return context.getString(R.string.inform_empty_user_name);
        }
        if (userService.addUser(qrScannerService.getUserId(), userName)){
            updateUserList();
            updateWidget();
            return String.format(context.getString(R.string.inform_user_is_created), userName);
        }
        return String.format(context.getString(R.string.inform_cannot_create_user), userName);
    }

    public void openAddUserDialog(){
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        View mView = layoutInflater.inflate(R.layout.user_input_dialog_box, null);
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
        alertDialogBuilder.setView(mView);

        final EditText editTextUserName = (EditText) mView.findViewById(R.id.userInputDialog);
        alertDialogBuilder
                .setCancelable(false)
                .setPositiveButton("Save",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialogBox, int id) {
                                Toast.makeText(context, userCreationResult(editTextUserName.getText().toString()), Toast.LENGTH_LONG).show();
                            }
                        })
                .setNegativeButton("Cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialogBox, int id) {
                                dialogBox.cancel();
                            }
                        });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }
}
