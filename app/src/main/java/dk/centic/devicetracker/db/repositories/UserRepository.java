package dk.centic.devicetracker.db.repositories;


import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import dk.centic.devicetracker.db.DbHelper;
import dk.centic.devicetracker.models.User;

//Users Table
public class UserRepository extends Repository implements IRepository {

    private static final String LOG_TAG = "Users table";

    private static final String USERS_TABLE_NAME = "USERS";

    private static final String KEY_USER_ID = "_id";
    private static final String PRIMARY_KEY = KEY_USER_ID;
    private static final String KEY_USER_NAME = "name";
    private static final String KEY_IS_USER_ACTIVE = "is_active";

    public UserRepository(Context context) {
        super(context);
    }

    @Override
    public RepositoryInfo getRepositoryInfo() {
        RepositoryInfo repositoryInfo = new RepositoryInfo();

        repositoryInfo.setTableName(USERS_TABLE_NAME);
        repositoryInfo.setPrimaryKeyName(PRIMARY_KEY);

        Map<String, String> tableFields = new HashMap<>();
        tableFields.put(KEY_USER_ID, DbHelper.TYPE_TEXT);
        tableFields.put(KEY_USER_NAME, DbHelper.TYPE_TEXT);
        tableFields.put(KEY_IS_USER_ACTIVE, DbHelper.TYPE_NUMERIC);
        repositoryInfo.setFields(tableFields);
        return repositoryInfo;
    }

    private User createUserByDBData(Cursor cursor){
        int idColInd = cursor.getColumnIndex(KEY_USER_ID);
        int nameColInd = cursor.getColumnIndex(KEY_USER_NAME);
        int isActiveColInd = cursor.getColumnIndex(KEY_IS_USER_ACTIVE);
        return new User(cursor.getString(idColInd), cursor.getString(nameColInd), cursor.getInt(isActiveColInd));
    }

    //add new user to repository
    public boolean addUser(String id, String name) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        long rowInserted;
        if (!checkIfRowExists(USERS_TABLE_NAME, KEY_USER_ID, id) && !checkIfRowExists(USERS_TABLE_NAME, KEY_USER_NAME, name)){
            ContentValues contentValues = new ContentValues();
            contentValues.put(KEY_USER_ID, id);
            contentValues.put(KEY_USER_NAME, name);
            if (isRepositoryEmpty(USERS_TABLE_NAME)){
                contentValues.put(KEY_IS_USER_ACTIVE, 1);
            } else {
                contentValues.put(KEY_IS_USER_ACTIVE, 0);
            }
            rowInserted = db.insert(USERS_TABLE_NAME, null, contentValues);
        } else{
            rowInserted = -1;
        }

        return rowInserted != -1;
    }

    public User getUserById(String userId){
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        Cursor cursor = null;
        try {
            cursor = db.query(USERS_TABLE_NAME, null, KEY_USER_ID + " = ?", new String[] {userId}, null, null, null);
            if(cursor.getCount() > 0) {
                cursor.moveToFirst();
                return createUserByDBData(cursor);
            }
        }finally {
            assert cursor != null;
            cursor.close();
        }
        return null;
    }

    // get all users
    public List<User> getUsers() {
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        Cursor cursor = db.query(USERS_TABLE_NAME, null, null, null, null, null, null);
        List<User> usersList = new ArrayList<User>();
        if (cursor.moveToFirst()) {
            do {
                User user = createUserByDBData(cursor);
                usersList.add(user);
            } while (cursor.moveToNext());

        } else {
            Log.d(LOG_TAG, "Table is empty!");
        }
        cursor.close();
        return usersList;
    }

    public boolean activateUserById(String userId){
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        //deactivate previous user
        deactivateUser();

        //activate user with userId
        ContentValues cv = new ContentValues();
        cv.put(KEY_IS_USER_ACTIVE, 1);
        long status = db.update(USERS_TABLE_NAME, cv, KEY_USER_ID + "=" + "'" + userId + "'", null);
        return status != -1;
    }

    public boolean deactivateUser(){
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        //deactivate previous user
        ContentValues contentValues = new ContentValues();
        contentValues.put(KEY_IS_USER_ACTIVE, 0);
        long status = db.update(USERS_TABLE_NAME, contentValues, KEY_IS_USER_ACTIVE + "=" + 1, null);
        return status != -1;
    }
}
