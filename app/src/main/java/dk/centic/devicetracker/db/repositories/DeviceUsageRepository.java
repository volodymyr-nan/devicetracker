package dk.centic.devicetracker.db.repositories;


import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import dk.centic.devicetracker.db.DbHelper;
import dk.centic.devicetracker.models.DeviceUsage;

public class DeviceUsageRepository extends Repository implements IRepository {

    private static final String LOG_TAG = "Device usage table";

    private static final String DEVICE_USAGE_TABLE_NAME = "DEVICE_USAGE";

    private static final String KEY_ID = "_id";
    private static final String PRIMARY_KEY = KEY_ID;
    private static final String KEY_USER_ID = "user_id";
    private static final String KEY_EVENT_TYPE = "event_type";
    private static final String KEY_TIMESTAMP = "timestamp";
    private static final String KEY_IS_SENT = "is_sent";

    public DeviceUsageRepository(Context context) {
        super(context);
    }

    @Override
    public RepositoryInfo getRepositoryInfo() {
        RepositoryInfo repositoryInfo = new RepositoryInfo();

        repositoryInfo.setTableName(DEVICE_USAGE_TABLE_NAME);
        repositoryInfo.setPrimaryKeyName(PRIMARY_KEY);

        Map<String, String> tableFields = new HashMap<>();
        tableFields.put(KEY_ID, DbHelper.TYPE_INTEGER);
        tableFields.put(KEY_USER_ID, DbHelper.TYPE_TEXT);
        tableFields.put(KEY_EVENT_TYPE, DbHelper.TYPE_INTEGER);
        tableFields.put(KEY_TIMESTAMP, DbHelper.TYPE_TEXT);
        tableFields.put(KEY_IS_SENT, DbHelper.TYPE_NUMERIC);

        repositoryInfo.setFields(tableFields);
        return repositoryInfo;
    }

    private DeviceUsage fromCursor(Cursor cursor) {
        // TODO: Create DeviceUsage object based on cursor data
        int idColInd = cursor.getColumnIndex(KEY_ID);
        int userIdColInd = cursor.getColumnIndex(KEY_USER_ID);
        int eventTypeColInd = cursor.getColumnIndex(KEY_EVENT_TYPE);
        int timestampTypeColInd = cursor.getColumnIndex(KEY_TIMESTAMP);
        int isSentColInd = cursor.getColumnIndex(KEY_IS_SENT);
        return new DeviceUsage(cursor.getInt(idColInd), cursor.getString(userIdColInd), cursor.getInt(eventTypeColInd), cursor.getString(timestampTypeColInd), cursor.getInt(isSentColInd));
    }

    public boolean createUsageReport(DeviceUsage report) {
        // TODO: create new db entry from device usage
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        long rowInserted;

        ContentValues contentValues = new ContentValues();
        //contentValues.put(KEY_ID, report.getId());
        contentValues.put(KEY_USER_ID, report.getUserId());
        contentValues.put(KEY_EVENT_TYPE, report.getEventType());
        contentValues.put(KEY_TIMESTAMP, report.getTimestamp());
        contentValues.put(KEY_IS_SENT, report.getSent());
        rowInserted = db.insert(DEVICE_USAGE_TABLE_NAME, null, contentValues);

        return rowInserted != -1;
    }

    public Collection<DeviceUsage> getUnsynchronizedReports() {
        // TODO: Return all unsynced reports
        List<DeviceUsage> unsyncedReports = new LinkedList<>();
        return unsyncedReports;
    }
}