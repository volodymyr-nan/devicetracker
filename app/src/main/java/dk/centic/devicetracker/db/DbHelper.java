package dk.centic.devicetracker.db;

import android.content.Context;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import dk.centic.devicetracker.db.repositories.DeviceUsageRepository;
import dk.centic.devicetracker.db.repositories.IRepository;
import dk.centic.devicetracker.db.repositories.RepositoryInfo;
import dk.centic.devicetracker.db.repositories.UserRepository;

public class DbHelper extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "UsersDB";
    private static final int DATABASE_VERSION = 1;

    //Types
    public static final String TYPE_TEXT = "text";
    public static final String TYPE_NUMERIC = "numeric";
    public static final String TYPE_INTEGER = "integer";

    //Table list
    public static List<IRepository> repositoryList = new ArrayList<>();

    public static void CreateDB(Context context){
        new UserRepository(context).AddToDB();
        new DeviceUsageRepository(context).AddToDB();
        new DbHelper(context).getReadableDatabase();
    }

    public DbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        for (IRepository table : repositoryList) {
            RepositoryInfo repositoryInfo = table.getRepositoryInfo();
            String execSQLQuery = "create table " + repositoryInfo.getTableName() +" (";
            for ( Map.Entry<String, String> entry : repositoryInfo.getFields().entrySet() ) {
                String key = entry.getKey();
                String value = entry.getValue();
                if (Objects.equals(key, repositoryInfo.getPrimaryKeyName())){
                    if (table instanceof DeviceUsageRepository){
                        execSQLQuery += key + " " + value + " primary key autoincrement not null,";
                    }
                    else {
                        execSQLQuery += key + " " + value + " primary key,";
                    }
                } else{
                    execSQLQuery += key + " " + value + ",";
                }
            }
            if (execSQLQuery.endsWith(",")) {
                execSQLQuery = execSQLQuery.substring(0, execSQLQuery.length() - 1);
            }
            execSQLQuery += ");";
            db.execSQL(execSQLQuery);
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        for (IRepository repository : repositoryList) {
            RepositoryInfo repositoryInfo = repository.getRepositoryInfo();
            db.execSQL("DROP TABLE IF EXISTS " + repositoryInfo.getTableName());
        }
        this.onCreate(db);
    }

    public ArrayList<Cursor> getData(String Query){
        //get writable database
        SQLiteDatabase sqlDB = this.getWritableDatabase();
        String[] columns = new String[] { "message" };
        //an array list of cursor to save two cursors one has results from the query
        //other cursor stores error message if any errors are triggered
        ArrayList<Cursor> alc = new ArrayList<Cursor>(2);
        MatrixCursor Cursor2= new MatrixCursor(columns);
        alc.add(null);
        alc.add(null);

        try{
            String maxQuery = Query ;
            //execute the query results will be save in Cursor c
            Cursor c = sqlDB.rawQuery(maxQuery, null);

            //add value to cursor2
            Cursor2.addRow(new Object[] { "Success" });

            alc.set(1,Cursor2);
            if (null != c && c.getCount() > 0) {

                alc.set(0,c);
                c.moveToFirst();

                return alc ;
            }
            return alc;
        } catch(SQLException sqlEx){
            Log.d("printing exception", sqlEx.getMessage());
            //if any exceptions are triggered save the error message to cursor an return the arraylist
            Cursor2.addRow(new Object[] { ""+sqlEx.getMessage() });
            alc.set(1,Cursor2);
            return alc;
        } catch(Exception ex){
            Log.d("printing exception", ex.getMessage());

            //if any exceptions are triggered save the error message to cursor an return the arraylist
            Cursor2.addRow(new Object[] { ""+ex.getMessage() });
            alc.set(1,Cursor2);
            return alc;
        }
    }
}
