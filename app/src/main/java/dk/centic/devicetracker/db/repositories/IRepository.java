package dk.centic.devicetracker.db.repositories;

public interface IRepository {
    RepositoryInfo getRepositoryInfo();
}
