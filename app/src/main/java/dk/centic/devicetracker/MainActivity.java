package dk.centic.devicetracker;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;

import com.google.zxing.integration.android.IntentIntegrator;

import dk.centic.devicetracker.db.DbHelper;
import dk.centic.devicetracker.db.repositories.DeviceUsageRepository;
import dk.centic.devicetracker.db.repositories.Repository;
import dk.centic.devicetracker.db.repositories.UserRepository;
import dk.centic.devicetracker.services.UIService;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    UIService uiService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setTitle("User list");

        DbHelper.CreateDB(this);

        ListView listView = (ListView) findViewById(R.id.listViewUsers);
        uiService = new UIService(this, listView);
        uiService.updateUserList();

        FloatingActionButton buttonScan = (FloatingActionButton) findViewById(R.id.buttonScan);
        buttonScan.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        uiService.startScanning();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (uiService.іsQRScanningSuccessful(IntentIntegrator.parseActivityResult(requestCode, resultCode, data))) {
            uiService.openAddUserDialog();
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    public void showDB(View view) {
        Intent dbmanager = new Intent(this, AndroidDatabaseManager.class);
        startActivity(dbmanager);
    }
}
