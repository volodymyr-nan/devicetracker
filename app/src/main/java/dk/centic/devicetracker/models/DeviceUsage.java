package dk.centic.devicetracker.models;

public class DeviceUsage {

    private int id;
    private String userId;
    private int eventType;
    private String timestamp;
    private Boolean isSent;

    public DeviceUsage(){}

    public DeviceUsage(int id, String userId, int eventType, String timestamp, int isSent) {
        setId(id);
        setUserId(userId);
        setEventType(eventType);
        setTimestamp(timestamp);
        setSent(isSent);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public int getEventType() {
        return eventType;
    }

    public void setEventType(int eventType) {
        this.eventType = eventType;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public Boolean getSent() {
        return isSent;
    }

    public void setSent(Integer sent) {
        isSent = sent > 0;
    }

    public void setSent(boolean sent) {
        isSent = sent;
    }
}
