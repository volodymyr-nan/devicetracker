package layout;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.widget.RemoteViews;
import android.widget.Toast;

import dk.centic.devicetracker.R;
import dk.centic.devicetracker.services.UserService;
import dk.centic.devicetracker.services.WidgetService;

public class UserListWidget extends AppWidgetProvider {

    final String ACTION_ON_CLICK = "dk.centic.devicetracker.listwidget.item_on_click";
    public static String ACTION_STOP_TRACKING = "dk.centic.devicetracker.stop_tracking";
    public final static String ITEM_USER_ID = "dk.centic.devicetracker.item_user_id";

    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        // There may be multiple widgets active, so update all of them
        super.onUpdate(context, appWidgetManager, appWidgetIds);
        for (int appWidgetId : appWidgetIds) {
            updateWidget(context, appWidgetManager, appWidgetId);
        }
    }

    @Override
    public void onEnabled(Context context) {
        // Enter relevant functionality for when the first widget is created
    }

    @Override
    public void onDisabled(Context context) {
        // Enter relevant functionality for when the last widget is disabled
    }

    void updateWidget(Context context, AppWidgetManager appWidgetManager, int appWidgetId) {
        RemoteViews remoteViews = new RemoteViews(context.getPackageName(), R.layout.user_list_widget);
        setTextViewClick(remoteViews, context, appWidgetId);
        setList(remoteViews, context);
        setListClick(remoteViews, context);
        appWidgetManager.updateAppWidget(appWidgetId, remoteViews);
        appWidgetManager.notifyAppWidgetViewDataChanged(appWidgetId, R.id.listViewUsersOnWidget);
    }

    void setTextViewClick(RemoteViews remoteViews, Context context, int appWidgetId) {
        Intent textViewClickIntent = new Intent(context, UserListWidget.class);
        textViewClickIntent.setAction(ACTION_STOP_TRACKING);
        textViewClickIntent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS, new int[] { appWidgetId });
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, appWidgetId, textViewClickIntent, 0);
        remoteViews.setOnClickPendingIntent(R.id.textViewStopTracking, pendingIntent);
    }

    void setList(RemoteViews remoteViews, Context context) {
        Intent adapterIntent = new Intent(context, WidgetService.class);
        remoteViews.setRemoteAdapter(R.id.listViewUsersOnWidget, adapterIntent);
    }

    void setListClick(RemoteViews remoteViews, Context context) {
        Intent listClickIntent = new Intent(context, UserListWidget.class);
        listClickIntent.setAction(ACTION_ON_CLICK);
        PendingIntent listClickPIntent = PendingIntent.getBroadcast(context, 0, listClickIntent, 0);
        remoteViews.setPendingIntentTemplate(R.id.listViewUsersOnWidget, listClickPIntent);
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        super.onReceive(context, intent);
        UserService userService = new UserService(context);
        if (intent.getAction().equalsIgnoreCase(ACTION_ON_CLICK)) {
            String userId = intent.getStringExtra(ITEM_USER_ID);
            if (!userId.isEmpty()) {
                String userName = userService.getUserById(userId).getName();
                if (userService.activateUserById(userId)){
                    Toast.makeText(context, String.format(context.getString(R.string.inform_user_is_activated), userName), Toast.LENGTH_SHORT).show();
                    //update widget
                    startUpdateWidget(context);
                } else {
                    Toast.makeText(context, String.format(context.getString(R.string.inform_cannot_activate_user), userName), Toast.LENGTH_SHORT).show();
                }
            }
        }

        if (intent.getAction().equals(ACTION_STOP_TRACKING)) {
            if (userService.stopTracking()){
                Toast.makeText(context, R.string.inform_tracking_is_stopped, Toast.LENGTH_SHORT).show();
                //update widget
                startUpdateWidget(context);
            }
            else {
                Toast.makeText(context, R.string.inform_cannot_stop_tracking, Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void startUpdateWidget(Context context){
        AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(context.getApplicationContext());
        ComponentName thisWidget = new ComponentName(context.getApplicationContext(), UserListWidget.class);
        int[] appWidgetIds = appWidgetManager.getAppWidgetIds(thisWidget);
        if (appWidgetIds != null && appWidgetIds.length > 0) {
            onUpdate(context, appWidgetManager, appWidgetIds);
        }
    }
}

